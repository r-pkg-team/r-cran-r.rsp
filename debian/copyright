Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: R.rsp
Upstream-Contact: Henrik Bengtsson <henrikb@braju.com>
Source: https://cran.r-project.org/package=R.rsp
Files-Excluded: build/vignette.rds inst/doc/*
Comment: Remove generated vignettes.

Files: *
Copyright: 2020 Henrik Bengtsson <henrikb@braju.com>
License: LGPL-2.1

Files: inst/tcl/r-httpd.tcl
Copyright: 2001-2003 Jonathan Hayward
           2002 Larry Smith
           2005 Tom Short
           2005 EPRI Solutions, Inc.
           2005 Henrick Bengtsson
License: GPL-2+ and Artistic
Comment: Jonathan Hayward's code released under Artistic License.  Others
 under GPL.

Files: debian/*
Copyright: 2021-2024 Doug Torrance <dtorrance@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation version 2.1 of the License, or (at your
 option) any later version.  This program is distributed in the hope that
 it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 .
 On Debian systems you can find the full text of the GNU Lesser General
 Public License version 2.1 license at /usr/share/common-licenses/LGPL-2.1.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: Artistic
 On Debian systems, the full text of the Artistic License can be found
 in the file `/usr/share/common-licenses/Artistic`.
